package eu.purplefriends.repositories

import eu.purplefriends.repositories.maven.MavenRepository

class GitlabPackageRegistry(
    apiUrl: String? = null,
) {
    val apiUrl = apiUrl ?: System.getenv("CI_API_V4_URL") ?: "https://gitlab.com/api/v4"
    fun mavenRepository(projectId: String): MavenRepository {
        return MavenRepository(
            mavenPackageRegistryUrl(projectId)
        )
    }

    fun mavenRepository(): MavenRepository? {
        val actualProjectId = System.getenv("CI_PROJECT_ID") ?: return null
        return MavenRepository(
            mavenPackageRegistryUrl(actualProjectId)
        )
    }

    private fun mavenPackageRegistryUrl(projectId: String): String {
        return "$apiUrl/projects/$projectId/packages/maven"
    }
}