plugins {
    `kotlin-dsl`

    id("maven-publish")
}

repositories {
    mavenCentral()
}

// required by maven-publish plugin
val group: String by project
val version: String by project

publishing {
    repositories {
        System.getenv("CI_JOB_TOKEN")?.let {
            maven(currentGitlabProjectRepositoryUrl()) {
                credentials(HttpHeaderCredentials::class) {
                    name = "Job-Token"
                    value = it
                }
                authentication {
                    create("header", HttpHeaderAuthentication::class)
                }
            }
        }
    }
}

fun currentGitlabProjectRepositoryUrl(): String {
    return "${System.getenv("CI_API_V4_URL")}/projects/${System.getenv("CI_PROJECT_ID")}/packages/maven"
}