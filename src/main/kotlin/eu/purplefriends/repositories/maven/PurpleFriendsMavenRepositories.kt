package eu.purplefriends.repositories.maven

import eu.purplefriends.repositories.GitlabPackageRegistry

object PurpleFriendsMavenRepositories {
    val mavenRepositoryRegistryRepository = GitlabPackageRegistry().mavenRepository("58049961")
    val gradleRepositoryAuthorizationMavenRepository = GitlabPackageRegistry().mavenRepository("57554022")
    val centralMavenRepository = GitlabPackageRegistry().mavenRepository("58068320")
}