package eu.purplefriends.repositories.maven

data class MavenRepository(val url: String)